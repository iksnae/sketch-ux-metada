#! /usr/local/bin/node

const fs = require('fs');
const { exec, spawn } = require('child_process');

let sketchFile = process.argv[2]


if (!fs.existsSync(sketchFile)) {
    process.stderr.write(`Failed to locate Sketch file named: ${sketchFile}\n`);
    return;
}

const readMeta = (file, cb) => {
    console.log("reading metadata..");
    exec(`unzip -p ${file} ux-meta.json`, (err, stdout, stderr) => {
        if (err) {
            console.log(`no UX Metadata available in "${sketchFile}"`);
        }else{
            cb(stdout);            
        }
    });
}

if (sketchFile == undefined) {
    console.log("require sketchfile to be passed as first argument")
    process.exit(1)
} else {
    readMeta(sketchFile, (results) => {
        console.log(JSON.parse(results));
    })
}
