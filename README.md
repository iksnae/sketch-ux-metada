# sketch-ux-metadata

> This is a very simple utility for inserting metadata into a sketchfile.


## Install

```bash
npm i -g sketch-ux-metadata
```

## Usage

Insert initial metadata file into Sketch file. If file already exists it will be overwritten.

```bash
> sketch-ux-metadata init example.sketch
```
Read the metadata from Sketch file.

```bash
> sketch-ux-metadata read example.sketch
```
Update metada in Sketch file with provided JSON string.

```bash
> sketch-ux-metadata update example.sketch '{ "version": "1.0.2"}'
```
