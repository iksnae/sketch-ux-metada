#!/usr/bin/env node

const { exec, spawn } = require('child_process');

const pkg = require('./package.json');
var program = require('commander');

program.version(pkg["version"]);

program.command("init")
    .usage('init <sketchfile>')
    .arguments("<sketchfile>", "The sketchfile to target")
    .action((sketchfile, env) => {
        exec(`sketch-ux-metadata-init ${sketchfile}`, (err, stdout, stderr) => {
            process.stderr.write(stderr);
            process.stdout.write(stdout);
        });
    });

program.command("read")
    .usage('read <sketchfile>')
    .arguments("<sketchfile>", "The sketchfile to target")
    .action((sketchfile, env) => {
        exec(`sketch-ux-metadata-read ${sketchfile}`, (err, stdout, stderr) => {
            process.stderr.write(stderr);
            process.stdout.write(stdout)
        });
    });

program.command("update")
    .usage('read <sketchfile> <metadata>')
    .arguments("<sketchfile>", "The sketchfile to target")
    .arguments("<metadata>", "The metadata to be updateed in sketchfile")
    .action((sketchfile, env) => {
        exec(`sketch-ux-metadata-update ${sketchfile} ${metadata}`, (err, stdout, stderr) => {
            process.stderr.write(stderr);
            process.stdout.write(stdout)
        });
    });

program.command("setval")
    .usage('setval <sketchfile> <key> <value>')
    .arguments("<sketchfile>", "The sketchfile to target")
    .arguments("<key>", "The key for value to be stored")
    .arguments("<value>", "The value to be stored")
    .action((sketchfile, key, value) => {
        exec(`sketch-ux-metadata-setval ${sketchfile} ${key} ${value}`, (err, stdout, stderr) => {
            process.stderr.write(stderr);
            process.stdout.write(stdout)
        });
    });

program.parse(process.argv);