#! /usr/local/bin/node

const fs = require('fs');
const { exec, spawn } = require('child_process');
const pkg = require('./package.json');

let sketchFile = process.argv[2];

if (!fs.existsSync(sketchFile)) {
    process.stderr.write(`Failed to locate Sketch file named: ${sketchFile}\n`);
    return;
}


let basicUXMetaData = {
    info: "UXProto-Metadata",
    version: pkg["version"],
    configuration: [],
    components: []
}

const content = JSON.stringify(basicUXMetaData, null, "");

const createTempFile = (cb) => {
    console.log("creating temp file..");
    fs.writeFileSync("ux-meta.json", content, { encoding: 'utf8', flag: 'w' });
    cb();
}

const deleteTempFile = (cb) => {
    console.log("deleting temp file..");
    fs.unlink("ux-meta.json", function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("temp file was deleted!");
        cb();
    });
}

const insertMeta = (cb) => {
    console.log("inserting metadata..");
    exec(`zip -pu ${sketchFile} ux-meta.json`, (err, stdout, stderr) => {
        if (err) {
            return;
        }
        cb();
    });
}

if (sketchFile == undefined) {
    console.log("require sketchfile to be passed as first argument")
    process.exit(1)
} else {
    createTempFile(() => {
        insertMeta(() => {
            deleteTempFile(() => {
                console.log("finished!")
            })
        })
    })
}