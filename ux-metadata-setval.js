#! /usr/local/bin/node

/**
 * 1. Open original metadata file as dictionary
 * 2. Set Value in parsed dictionary
 * 3. Save updated dictionary
 */


const fs = require('fs');
const { exec, spawn } = require('child_process');

let sketchFile = process.argv[2];
let key = process.argv[3];
let value = process.argv[4];


if (!fs.existsSync(sketchFile)) {
    process.stderr.write(`Failed to locate Sketch file named: ${sketchFile}\n`);
    return;
}

const openMetadata = (cb) => {
    console.log("opening metadata file...");
    exec(`unzip -p ${sketchFile} ux-meta.json`, (err, stdout, stderr) => {
        if (err) {
            process.stderr.write(err);
            process.exit(1);
        }else{
            var metadata = JSON.parse(stdout);
            metadata[key] = value;
            var updated_metadata = JSON.stringify(metadata);
            console.log(updated_metadata);
            cb(updated_metadata);
        }
    });
}



const createTempFile = (jsonData, cb)=>{
    console.log("creating temp file..");
    fs.writeFileSync("ux-meta.json", jsonData, {encoding:'utf8',flag:'w'}); 
    cb();
}

const deleteTempFile = (cb) => {
    console.log("deleting temp file..");
    fs.unlink("ux-meta.json", function (err) {
        if (err) {
            process.stderr.write(err);
            process.exit(1);
        }
        console.log("The file was deleted!");
        cb();
    }); 
}

const insertMeta = (cb) => {
    console.log("updating metadata..");
    exec(`zip -pu ${sketchFile} ux/meta.json`, (err, stdout, stderr) => {
        if (err) {
        process.stderr.write(err);
          process.exit(1);
        }
        cb();
      });
}

if (key == undefined){
    process.exit(1)
}

if (value == undefined){
    process.exit(2)
}
if(sketchFile == undefined){
    process.exit(3)
}
openMetadata((metadata)=>{
    createTempFile(metadata, ()=>{
        insertMeta(()=>{
            deleteTempFile(()=>{
                console.log("finished!")
            })
        })
    })
})