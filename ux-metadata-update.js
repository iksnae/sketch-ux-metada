#! /usr/local/bin/node

const fs = require('fs');
const { exec, spawn } = require('child_process');

let sketchFile = process.argv[2];
let jsonData = process.argv[3];

if (!fs.existsSync(sketchFile)) {
    process.stderr.write(`Failed to locate Sketch file named: ${sketchFile}\n`);
    return;
}

const createTempFile = (cb)=>{
    console.log("creating temp file..");
    fs.writeFileSync("./ux-meta.json", jsonData, {encoding:'utf8',flag:'w'}); 
    cb();
}

const deleteTempFile = (cb) => {
    console.log("deleting temp file..");
    fs.unlink("./ux-meta.json", function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("The file was deleted!");
        cb();
    }); 
}

const insertMeta = (cb) => {
    console.log("updating metadata..");
    exec(`zip -pu ${sketchFile} ux-meta.json`, (err, stdout, stderr) => {
        if (err) {
          return;
        }
        cb();
      });
}

if(sketchFile == undefined){
    console.log("require sketchfile to be passed as first argument")
    process.exit(1)
}else{
    createTempFile(()=>{
        insertMeta(()=>{
            deleteTempFile(()=>{
                console.log("finished!")
            })
        })
    })
}